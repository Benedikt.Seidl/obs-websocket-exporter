use std::fmt::Write;
use std::sync::Mutex;

use crate::communication::PromSide;
use crate::communication::{MessageToObs, MessageToProm};
use crate::config::ConfigProm;
use crate::obs::request;
use crate::obs::response;

use actix_web::{get, web, App, HttpServer, Responder};
use anyhow::Context;
use async_channel::{Receiver, Sender};

pub struct Prometheus {
    sender_to_obs: Sender<MessageToObs>,
    receiver_to_prom: Receiver<MessageToProm>,
    config: ConfigProm,
}

pub struct AppState {
    prom: Mutex<Prometheus>,
}

enum MetricDefinitionValue {
    Counter(u64),
    Gauge(f64),
}

fn write_metric(
    result: &mut String,
    name: &str,
    value: MetricDefinitionValue,
    tags: &Vec<(&str, &str)>,
) -> Result<(), anyhow::Error> {
    let (metric_type, value) = match value {
        MetricDefinitionValue::Gauge(value) => ("gauge", format!("{value}")),
        MetricDefinitionValue::Counter(value) => ("counter", format!("{value}")),
    };
    let mut tags_formatted = String::new();
    if tags.len() > 0 {
        write!(tags_formatted, "{{")?;
        for (key, value) in tags.iter() {
            write!(tags_formatted, "{key}=\"{value}\",")?;
        }
        tags_formatted.pop(); // remove last ,
        write!(tags_formatted, "}}")?;
    }
    // TODO: wait for https://github.com/prometheus/client_rust/pull/82
    // to not use simple string formatting, but a library
    write!(
        result,
        "# TYPE {name} {metric_type}\n{name}{tags_formatted} {value}\n"
    )?;
    Ok(())
}

macro_rules! create_function {
    ($func_name:ident, $response_match:path, $response_type:ty) => {
        async fn $func_name(
            app_state: &Prometheus,
            request: MessageToObs,
        ) -> Result<$response_type, anyhow::Error> {
            app_state
                .sender_to_obs
                .send(request)
                .await
                .context(format!(
                    "{:?}: can not send message from prom to obs",
                    stringify!($func_name)
                ))?;
            let result = app_state.receiver_to_prom.recv().await?;
            return match result {
                $response_match(data) => match data {
                    Ok(data) => Ok(data),
                    Err(error) => Err(anyhow!("{}", error)),
                },
                _ => panic!(
                    "expected data of type {:?}, got {:?}",
                    stringify!($response_match),
                    result
                ),
            };
        }
    };
}

create_function!(
    get_output_list,
    MessageToProm::GetOutputListResponse,
    response::GetOutputList
);
create_function!(
    get_output_status,
    MessageToProm::GetOutputStatusResponse,
    response::GetOutputStatus
);
create_function!(
    get_stats,
    MessageToProm::GetStatsResponse,
    response::GetStats
);
create_function!(
    get_owe_status,
    MessageToProm::GetOweStatusResponse,
    response::GetOweStatus
);

async fn _metrics(data: web::Data<AppState>) -> Result<String, anyhow::Error> {
    // async closure are unstable, perhaps there would be a more clever way?
    let app_state = data
        .prom
        .lock()
        .or_else(|e| Err(anyhow!("Could not aquire lock {:?}", e)))?;

    let mut result = String::new();

    let owe_status = get_owe_status(&app_state, MessageToObs::GetOweStatus).await?;
    write_metric(
        &mut result,
        "owe_owe_reconnect_count_total",
        MetricDefinitionValue::Counter(owe_status.reconnect_count),
        &vec![],
    )?;
    write_metric(
        &mut result,
        "owe_owe_running_for_seconds",
        MetricDefinitionValue::Counter(owe_status.running_for_seconds),
        &vec![],
    )?;
    write_metric(
        &mut result,
        "owe_owe_connected_for_seconds",
        MetricDefinitionValue::Counter(owe_status.connected_for_seconds),
        &vec![],
    )?;

    match get_stats(&app_state, MessageToObs::GetStats).await {
        Ok(get_stats) => {
            write_metric(
                &mut result,
                "owe_obs_GetStats_renderTotalFrames",
                MetricDefinitionValue::Counter(get_stats.renderTotalFrames),
                &vec![],
            )?;
            write_metric(
                &mut result,
                "owe_obs_GetStats_renderSkippedFrames",
                MetricDefinitionValue::Counter(get_stats.renderSkippedFrames),
                &vec![],
            )?;
            write_metric(
                &mut result,
                "owe_obs_GetStats_outputTotalFrames",
                MetricDefinitionValue::Counter(get_stats.outputTotalFrames),
                &vec![],
            )?;
            write_metric(
                &mut result,
                "owe_obs_GetStats_outputSkippedFrames",
                MetricDefinitionValue::Counter(get_stats.outputSkippedFrames),
                &vec![],
            )?;
            write_metric(
                &mut result,
                "owe_obs_GetStats_averageFrameRederTime_seconds",
                MetricDefinitionValue::Gauge(get_stats.averageFrameRenderTime / 1000.),
                &vec![],
            )?;
            write_metric(
                &mut result,
                "owe_obs_GetStats_activeFps",
                MetricDefinitionValue::Gauge(get_stats.activeFps),
                &vec![],
            )?;

            let output_list = get_output_list(&app_state, MessageToObs::GetOutputList).await?;
            for output in output_list.outputs {
                let tags: Vec<(&str, &str)> = vec![("outputName", &output.outputName)];
                write_metric(
                    &mut result,
                    "owe_obs_GetOutputList_outputHeight",
                    MetricDefinitionValue::Gauge(output.outputHeight as f64),
                    &tags,
                )?;

                write_metric(
                    &mut result,
                    "owe_obs_GetOutputList_outputWidth",
                    MetricDefinitionValue::Gauge(output.outputWidth as f64),
                    &tags,
                )?;

                let status = get_output_status(
                    &app_state,
                    MessageToObs::GetOutputStatus(request::GetOutputStatus {
                        outputName: output.outputName.clone(),
                    }),
                )
                .await?;

                write_metric(
                    &mut result,
                    "owe_obs_GetOutputStatus_outputActive",
                    MetricDefinitionValue::Gauge(match status.outputActive {
                        true => 1.0,
                        false => 0.0,
                    }),
                    &tags,
                )?;

                write_metric(
                    &mut result,
                    "owe_obs_GetOutputStatus_outputReconnecting",
                    MetricDefinitionValue::Gauge(match status.outputReconnecting {
                        true => 1.0,
                        false => 0.0,
                    }),
                    &tags,
                )?;

                write_metric(
                    &mut result,
                    "owe_obs_GetOutputStatus_outputDuration",
                    MetricDefinitionValue::Gauge(status.outputDuration),
                    &tags,
                )?;

                write_metric(
                    &mut result,
                    "owe_obs_GetOutputStatus_outputCongestion",
                    MetricDefinitionValue::Gauge(status.outputCongestion),
                    &tags,
                )?;

                write_metric(
                    &mut result,
                    "owe_obs_GetOutputStatus_outputBytes",
                    MetricDefinitionValue::Counter(status.outputBytes),
                    &tags,
                )?;

                write_metric(
                    &mut result,
                    "owe_obs_GetOutputStatus_outputSkippedFrames",
                    MetricDefinitionValue::Counter(status.outputSkippedFrames),
                    &tags,
                )?;

                write_metric(
                    &mut result,
                    "owe_obs_GetOutputStatus_outputTotalFrames",
                    MetricDefinitionValue::Counter(status.outputTotalFrames),
                    &tags,
                )?;
            }
        }
        Err(err) => {
            error!("could not load stats {:?}", err)
        }
    };

    return Ok(result);
}

#[get("/metrics")]
async fn metrics(data: web::Data<AppState>) -> impl Responder {
    match _metrics(data).await {
        Ok(result) => actix_web::HttpResponse::Ok().body(result),
        Err(_) => return actix_web::HttpResponse::BadRequest().body("ERROR!"),
    }
}

impl Prometheus {
    pub fn new(config: ConfigProm, communication: PromSide) -> Prometheus {
        let (sender_to_obs, receiver_to_prom) = communication;
        Prometheus {
            config,
            sender_to_obs,
            receiver_to_prom,
        }
    }

    pub async fn main(self) -> Result<(), anyhow::Error> {
        let bind_address = self.config.bind.clone();
        let bind_port = self.config.port;

        let data = web::Data::new(AppState {
            prom: Mutex::new(self),
        });

        HttpServer::new(move || App::new().app_data(data.clone()).service(metrics))
            .bind((bind_address, bind_port))?
            .run()
            .await?;
        Ok(())
    }
}
