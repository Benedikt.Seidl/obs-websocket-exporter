mod communication;
mod config;
mod obs;
mod prometheus;

use crate::config::load;
use crate::obs::Obs;
use crate::prometheus::Prometheus;

use anyhow::Context;
use async_channel::bounded;
use futures_util::select;
use futures_util::FutureExt;

#[macro_use]
extern crate log;

#[macro_use]
extern crate anyhow;

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    env_logger::init();

    let config = load().context("Failed to load config")?;
    // we need to pass messages between obs and prometheus part:

    //============//                                 //============//
    //    OBS     //                                 // Prometheus //
    //            //         MessageToProm           //            //
    // to_prom_tx // ---------------------------->>> // to_prom_rx //
    //            //                                 //            //
    //            //         MessageToObs            //            //
    //  to_obs_rx // <<<---------------------------- // to_obs_tx  //
    //            //                                 //            //
    //============//                                 //============//
    let (to_obs_tx, to_obs_rx) = bounded(1);
    let (to_prom_tx, to_prom_rx) = bounded(1);

    let mut obs = Obs::new(config.obs, (to_prom_tx, to_obs_rx)).await?;
    let prometheus = Prometheus::new(config.prom, (to_obs_tx, to_prom_rx));

    select! {
        result = prometheus.main().fuse() => result,
        result = obs.main().fuse() => result,
    }
}
