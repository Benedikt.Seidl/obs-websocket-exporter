#![allow(non_snake_case)]

use serde::Serialize;

#[derive(Debug, Serialize)]
pub struct GetOutputStatus {
    pub outputName: String,
}
