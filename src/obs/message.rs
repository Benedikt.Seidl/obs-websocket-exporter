#![allow(non_snake_case)]

use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize)]
pub struct HelloData {
    pub obsWebSocketVersion: String,
    pub rpcVersion: i32,
    pub authentication: Option<HelloDataAuthentication>,
}

#[derive(Debug, Deserialize)]
pub struct HelloDataAuthentication {
    pub challenge: String,
    pub salt: String,
}

#[derive(Debug, Deserialize)]
pub struct IdentifiedData {
    #[allow(dead_code)]
    negotiatedRpcVersion: i32,
}

#[derive(Debug, Serialize)]
pub struct IdentifyData {
    pub rpcVersion: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub authentication: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub eventSubscriptions: Option<i32>,
}

#[derive(Debug, Serialize)]
pub struct RequestData {
    pub requestType: String,
    pub requestId: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub requestData: Option<serde_json::Value>
}

#[derive(Debug)]
pub enum Response {
    Hello(HelloData),
    Identified(IdentifiedData),
    Nop(),
}

#[derive(Debug, Serialize)]
#[serde(untagged)]
pub enum MessageType {
    Identify(IdentifyData),
    Request(RequestData),
}

// TODO: rename
#[derive(Debug, Serialize)]
pub struct Wrapper {
    pub op: i32,
    pub d: MessageType,
}
