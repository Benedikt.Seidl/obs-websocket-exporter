pub mod message;
pub mod request;
pub mod response;

use std::collections::HashMap;

use anyhow::Context;
use base64::encode;
use futures_util::stream::{SplitSink, SplitStream};
use futures_util::FutureExt;
use futures_util::StreamExt;
use futures_util::{select, SinkExt};
use http::Uri;
use serde::Deserialize;
use serde_json;
use serde_json::Value;
use sha2::{Digest, Sha256};
use tokio_tungstenite::WebSocketStream;
use tokio_tungstenite::{connect_async, tungstenite::protocol::Message};
use uuid::Uuid;

use crate::communication::{MessageToObs, MessageToProm, ObsSide};
use crate::config::ConfigObs;
use crate::obs::message::{
    HelloData, IdentifiedData, IdentifyData, MessageType, RequestData, Response, Wrapper,
};
use async_channel::{Receiver, Sender};
use std::time::Instant;

type ReadWs =
    Option<SplitStream<WebSocketStream<tokio_tungstenite::MaybeTlsStream<tokio::net::TcpStream>>>>;

pub struct Obs {
    config: ConfigObs,
    websocket_uri: Uri,
    websocket_write: Option<
        SplitSink<
            WebSocketStream<tokio_tungstenite::MaybeTlsStream<tokio::net::TcpStream>>,
            Message,
        >,
    >,
    websocket_read: ReadWs,
    sender_to_prom: Sender<MessageToProm>,
    receiver_to_obs: Receiver<MessageToObs>,
    // TODO: add timeout, so elements are cleaned up!
    waiting_for_response: HashMap<Uuid, MessageToObs>,
    websocket_reconnect_count: u64,
    websocket_connection_started: Option<Instant>,
    started: Instant,
}

async fn websocket_read_next(read: &mut ReadWs) -> Result<Tick, anyhow::Error> {
    return match read {
        Some(read) => match read.next().fuse().await {
            Some(message) => match message {
                Ok(m) => Ok(Tick::Websocket(Ok(m))),
                Err(e) => Err(anyhow!("message error {:?}", e)),
            },
            None => {
                warn!("read error");
                debug!("sleep 1 second");
                tokio::time::sleep(std::time::Duration::from_secs(1)).await;
                Ok(Tick::ConnectionLost)
            }
        },
        None => {
            warn!("not connected");
            debug!("sleep 1 second");
            tokio::time::sleep(std::time::Duration::from_secs(1)).await;
            Ok(Tick::ConnectionLost)
        }
    };
}

enum Tick {
    Websocket(Result<Message, anyhow::Error>),
    ConnectionLost,
    Command(MessageToObs),
}

fn get_response_data(value: serde_json::Value) -> Result<serde_json::Value, anyhow::Error> {
    Ok(value
        .get("d")
        .ok_or_else(|| anyhow!("'d' not found"))?
        .get("responseData")
        .ok_or_else(|| anyhow!("'d.ResponseData' not found"))?
        .clone())
}

impl Obs {
    pub async fn new(config: ConfigObs, communication: ObsSide) -> Result<Obs, anyhow::Error> {
        let (sender_to_prom, receiver_to_obs) = communication;
        let uri = Uri::builder()
            .scheme("ws")
            .authority(format!("{}:{}", config.host, config.port))
            .path_and_query("/")
            .build()
            .context("Could not build websocket address")?;

        Ok(Obs {
            config,
            sender_to_prom,
            receiver_to_obs,
            waiting_for_response: HashMap::new(),
            websocket_uri: uri,
            websocket_write: None,
            websocket_read: None,
            websocket_reconnect_count: 0,
            websocket_connection_started: None,
            started: Instant::now(),
        })
    }

    async fn websocket_ensure_connected(&mut self) -> Result<(), anyhow::Error> {
        if self.websocket_write.is_none() {
            self.websocket_reconnect_count += 1;
            debug!("trying to connect to {:?}", &self.websocket_uri);
            let (ws_stream, _) = connect_async(&self.websocket_uri).await?;
            let (write, read) = ws_stream.split();
            self.websocket_write = Some(write);
            self.websocket_read = Some(read);
            self.websocket_connection_started = Some(Instant::now());
        }
        Ok(())
    }

    async fn websocket_write_send(&mut self, message: Message) -> Result<(), anyhow::Error> {
        self.websocket_ensure_connected().await?;
        return match &mut self.websocket_write {
            Some(write) => {
                write.send(message).await?;
                return Ok(());
            }
            None => Err(anyhow!("Can not write, websocket connection not open.")),
        };
    }

    async fn tick(&mut self) -> Result<Tick, anyhow::Error> {
        select! {
            websocket = websocket_read_next(&mut self.websocket_read).fuse() => websocket,
            command = self.receiver_to_obs.recv().fuse() => Ok(Tick::Command(command?)),
        }
    }

    pub async fn handle_websocket_message(
        &mut self,
        message: Message,
    ) -> Result<(), anyhow::Error> {
        match message {
            Message::Close(message) => {
                warn!("websocket connection closed: {:?}", message);
                // nothing to do here, websocket.read() will fail next
                // and handle reconnection
            }
            Message::Text(message) => {
                debug!("received {}", message);
                // TODO: XXX: move this into a function
                // and write some tests for this function!
                let value: Value = serde_json::from_str(&message)?;

                let response = match value
                    .get("op")
                    .and_then(Value::as_u64)
                    .ok_or_else(|| anyhow!("could not read op on message from OBS"))?
                {
                    // rewrite after https://github.com/serde-rs/serde/issues/745 is resolved
                    0 => Response::Hello(HelloData::deserialize(
                        value
                            .get("d")
                            .ok_or_else(|| anyhow!("Could not read 'd' on Response::Hello"))?,
                    )?),
                    2 => Response::Identified(IdentifiedData::deserialize(
                        value
                            .get("d")
                            .ok_or_else(|| anyhow!("Could not read 'd' on Response::Identified"))?,
                    )?),
                    7 => {
                        // TODO: check for requestStatus
                        let uuid = Uuid::parse_str(
                            value
                                .get("d")
                                .ok_or_else(|| anyhow!("'d' not found on RequestResponse"))?
                                .get("requestId")
                                .ok_or_else(|| {
                                    anyhow!("'d.requestId' not found on RequestResponse")
                                })?
                                .as_str()
                                .ok_or_else(|| anyhow!("'d.requestId' not a string"))?,
                        )
                        .context("Failed to parse uuid of RequestResponse")?;
                        debug!("received message for uuid {}", uuid);

                        match self
                            .waiting_for_response
                            .remove(&uuid)
                            .context("Could not find response requesting that uuid")?
                        {
                            MessageToObs::GetOutputList => {
                                let response = MessageToProm::GetOutputListResponse(Ok(
                                    response::GetOutputList::deserialize(
                                        get_response_data(value).context("GetOutputList")?,
                                    )?,
                                ));
                                self.sender_to_prom.send(response).await?;
                            }
                            MessageToObs::GetOutputStatus(_) => {
                                let response = MessageToProm::GetOutputStatusResponse(Ok(
                                    response::GetOutputStatus::deserialize(
                                        get_response_data(value).context("GetOutputStatus")?,
                                    )?,
                                ));
                                self.sender_to_prom.send(response).await?;
                            }
                            MessageToObs::GetStats => {
                                let response = MessageToProm::GetStatsResponse(Ok(
                                    response::GetStats::deserialize(
                                        get_response_data(value).context("GetStats")?,
                                    )?,
                                ));
                                self.sender_to_prom.send(response).await?;
                            }
                            MessageToObs::GetOweStatus => {
                                // nothing to do here, this never happens.
                                panic!("this should never happen");
                            }
                        }
                        Response::Nop()
                    }
                    op => bail!("unsupported op {:?}", op),
                };
                self.handle(response).await?;
            }
            message => {
                bail!("did not expect message {:?}", message)
            }
        };
        Ok(())
    }

    pub async fn main(&mut self) -> Result<(), anyhow::Error> {
        loop {
            match self.tick().await? {
                Tick::ConnectionLost => {
                    self.websocket_write = None;
                    self.websocket_read = None;
                    self.websocket_connection_started = None;
                    match self.websocket_ensure_connected().await {
                        Err(e) => {
                            warn!("could not connect {:?}", e);
                        }
                        _ => {}
                    }
                }
                Tick::Command(command) => {
                    debug!("command {:?}", command);
                    match command {
                        MessageToObs::GetStats => {
                            self.request_stats().await?;
                        }
                        MessageToObs::GetOutputList => {
                            self.request_output_list().await?;
                        }
                        MessageToObs::GetOutputStatus(request) => {
                            self.request_output_status(request).await?;
                        }
                        MessageToObs::GetOweStatus => {
                            self.request_owe_status().await?;
                        }
                    }
                }
                Tick::Websocket(message) => match message {
                    Ok(message) => self.handle_websocket_message(message).await?,
                    Err(_) => {
                        debug!("error reading, waiting for reconnect");
                        tokio::time::sleep(std::time::Duration::from_secs(1)).await;
                    }
                },
            }
        }
    }

    async fn handle(&mut self, response: Response) -> Result<(), anyhow::Error> {
        // TODO: this function seems strange. why only react on hello, but nothing else?
        // could this be refactored so the handle function can be removed?
        match response {
            Response::Hello(hello) => {
                self.identify(hello).await?;
            }
            _ => {
                warn!("ignored message: {:?}", response);
            }
        }
        Ok(())
    }

    async fn send(&mut self, request: MessageType) -> Result<(), anyhow::Error> {
        let text = serde_json::to_string(&Wrapper {
            op: match request {
                MessageType::Identify(_) => 1,
                MessageType::Request(_) => 6,
            },
            d: request,
        })?;
        debug!("send: {}", text);
        self.websocket_write_send(Message::Text(text)).await?;
        Ok(())
    }

    // TODO: create macro to unify request_output_list and request_status
    async fn request_output_status(
        &mut self,
        request: request::GetOutputStatus,
    ) -> Result<(), anyhow::Error> {
        let uuid = Uuid::new_v4();
        self.send(MessageType::Request(RequestData {
            requestType: "GetOutputStatus".into(),
            requestId: uuid.to_string(),
            requestData: Some(serde_json::to_value(&request)?),
        }))
        .await?;
        self.waiting_for_response
            .insert(uuid, MessageToObs::GetOutputStatus(request));
        Ok(())
    }
    async fn request_output_list(&mut self) -> Result<(), anyhow::Error> {
        let uuid = Uuid::new_v4();
        self.send(MessageType::Request(RequestData {
            requestType: "GetOutputList".into(),
            requestId: uuid.to_string(),
            requestData: None,
        }))
        .await?;
        self.waiting_for_response
            .insert(uuid, MessageToObs::GetOutputList);
        Ok(())
    }
    async fn request_stats(&mut self) -> Result<(), anyhow::Error> {
        let uuid = Uuid::new_v4();
        match self
            .send(MessageType::Request(RequestData {
                requestType: "GetStats".into(),
                requestId: uuid.to_string(),
                requestData: None,
            }))
            .await
        {
            // TODO: do same error handline for other two requests, or build
            // macro?!
            Ok(_) => {}
            Err(err) => {
                self.sender_to_prom
                    .send(MessageToProm::GetStatsResponse(Err(err)))
                    .await?;
            }
        }
        self.waiting_for_response
            .insert(uuid, MessageToObs::GetStats);
        Ok(())
    }
    async fn request_owe_status(&mut self) -> Result<(), anyhow::Error> {
        self.sender_to_prom
            .send(MessageToProm::GetOweStatusResponse(Ok(
                response::GetOweStatus {
                    reconnect_count: self.websocket_reconnect_count,
                    running_for_seconds: self.started.elapsed().as_secs(),
                    connected_for_seconds: match self.websocket_connection_started {
                        None => 0,
                        Some(instant) => instant.elapsed().as_secs(),
                    },
                },
            )))
            .await?;
        Ok(())
    }

    async fn identify(&mut self, hello: HelloData) -> Result<(), anyhow::Error> {
        let mut hasher = Sha256::new();
        hasher.update(self.config.password.as_ref().ok_or_else(|| anyhow!("01"))?);
        hasher.update(
            &hello
                .authentication
                .as_ref()
                .ok_or_else(|| anyhow!("02"))?
                .salt,
        );
        let base64_secret = encode(hasher.finalize());

        let mut hasher = Sha256::new();
        hasher.update(base64_secret);
        hasher.update(
            &hello
                .authentication
                .as_ref()
                .ok_or_else(|| anyhow!("03"))?
                .challenge,
        );
        let authentication = encode(hasher.finalize());

        self.send(MessageType::Identify(IdentifyData {
            rpcVersion: 1,
            authentication: Some(authentication),
            eventSubscriptions: Some(0),
            // TODO would be cool to have a event for reconnecting, then we
            // could count the reconnects.
        }))
        .await?;
        Ok(())
    }
}
