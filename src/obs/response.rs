#![allow(non_snake_case)]

use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct GetStats {
    pub activeFps: f64,
    pub availableDiskSpace: f64,
    pub averageFrameRenderTime: f64,
    pub cpuUsage: f64,
    pub memoryUsage: f64,
    pub outputSkippedFrames: u64,
    pub outputTotalFrames: u64,
    pub renderSkippedFrames: u64,
    pub renderTotalFrames: u64,
    pub webSocketSessionIncomingMessages: u64,
    pub webSocketSessionOutgoingMessages: u64,
}

#[derive(Debug, Deserialize)]
pub struct GetOutputListOutputFlags {
    pub OBS_OUTPUT_AUDIO: bool,
    pub OBS_OUTPUT_ENCODED: bool,
    pub OBS_OUTPUT_MULTI_TRACK: bool,
    pub OBS_OUTPUT_SERVICE: bool,
    pub OBS_OUTPUT_VIDEO: bool,
}

#[derive(Debug, Deserialize)]
pub struct GetOutputListOutput {
    pub outputActive: bool,
    pub outputHeight: u64,
    pub outputWidth: u64,
    pub outputKind: String,
    pub outputName: String,
    pub outputFlags: GetOutputListOutputFlags,
}

#[derive(Debug, Deserialize)]
pub struct GetOutputList {
    pub outputs: Vec<GetOutputListOutput>,
}

// ---

#[derive(Debug, Deserialize)]
pub struct GetOutputStatus {
    pub outputActive: bool,
    pub outputReconnecting: bool,
    pub outputTimecode: String,
    pub outputDuration: f64,
    pub outputCongestion: f64,
    pub outputBytes: u64,
    pub outputSkippedFrames: u64,
    pub outputTotalFrames: u64,
}

// ---

#[derive(Debug, Deserialize)]
pub struct GetOweStatus {
    pub reconnect_count: u64,
    pub running_for_seconds: u64,
    pub connected_for_seconds: u64,
}
