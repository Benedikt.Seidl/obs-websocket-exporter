use serde::Deserialize;
use std::fs::File;
use std::io;
use std::io::prelude::*;

#[derive(Deserialize)]
pub struct Config {
    pub obs: ConfigObs,
    pub prom: ConfigProm
}

#[derive(Deserialize)]
pub struct ConfigObs {
    pub host: String,
    pub port: u16,
    pub password: Option<String>,
}

#[derive(Deserialize)]
pub struct ConfigProm {
    pub bind: String,
    pub port: u16,
}

pub fn load() -> io::Result<Config> {
    let mut file = File::open("owe-config.toml")?;

    let mut content = String::new();
    file.read_to_string(&mut content)?;
    let config: Config = toml::from_str(&content)?;
    return Ok(config);
}
