use async_channel::{Receiver, Sender};

use crate::obs::response;
use crate::obs::request;

pub type PromSide = (Sender<MessageToObs>, Receiver<MessageToProm>);
pub type ObsSide = (Sender<MessageToProm>, Receiver<MessageToObs>);

#[derive(Debug)]
pub enum MessageToObs {
    GetStats,
    GetOutputList,
    GetOutputStatus(request::GetOutputStatus),
    GetOweStatus,
}

#[derive(Debug)]
pub enum MessageToProm {
    GetStatsResponse(Result<response::GetStats, anyhow::Error>),
    GetOutputListResponse(Result<response::GetOutputList, anyhow::Error>),
    GetOutputStatusResponse(Result<response::GetOutputStatus, anyhow::Error>),
    GetOweStatusResponse(Result<response::GetOweStatus, anyhow::Error>),
}
