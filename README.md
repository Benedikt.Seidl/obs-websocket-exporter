# obs-websocket-exporter

This is a standalone tool, that uses the [obs-websocket][ow] plugin (version
5.0.0 or greater) to export metrics from [obs-studio][obs] into
[prometheus][prom].

[ow]: https://github.com/obsproject/obs-websocket/
[obs]: https://obsproject.com/
[prom]: https://prometheus.io/


## How to use

Create a file `owe-config.toml` in the directory you want to start
`obs-websocket-exporter`.

```toml
[obs]
host = "127.0.0.1"
port = 4444
password = "EYshJT0CwnX83xmd"
[prom]
bind = "0.0.0.0"
port = 9090
```

In order to enable logging you can set the environment variable `RUST_LOG`, for
example: `RUST_LOG=debug` to understand what obs-websocket-exporter is
currently doing.

## What to expect

This is one of my first rust projects, so expect some rough edges. Probably
there will be some breaking changes in the metric names, or configuration.

Currently the following metrics are exported.

```
# TYPE owe_owe_reconnect_count_total counter
owe_owe_reconnect_count_total 71
# TYPE owe_owe_running_for_seconds counter
owe_owe_running_for_seconds 710
# TYPE owe_owe_connected_for_seconds counter
owe_owe_connected_for_seconds 639
# TYPE owe_obs_GetStats_renderTotalFrames counter
owe_obs_GetStats_renderTotalFrames 19216
# TYPE owe_obs_GetStats_renderSkippedFrames counter
owe_obs_GetStats_renderSkippedFrames 8
# TYPE owe_obs_GetStats_outputTotalFrames counter
owe_obs_GetStats_outputTotalFrames 6006
# TYPE owe_obs_GetStats_outputSkippedFrames counter
owe_obs_GetStats_outputSkippedFrames 0
# TYPE owe_obs_GetStats_averageFrameRederTime_seconds gauge
owe_obs_GetStats_averageFrameRederTime_seconds 0.0013196570000000001
# TYPE owe_obs_GetStats_activeFps gauge
owe_obs_GetStats_activeFps 30.000000300000007
# TYPE owe_obs_GetOutputList_outputHeight gauge
owe_obs_GetOutputList_outputHeight{outputName="adv_stream"} 720
# TYPE owe_obs_GetOutputList_outputWidth gauge
owe_obs_GetOutputList_outputWidth{outputName="adv_stream"} 1280
# TYPE owe_obs_GetOutputStatus_outputActive gauge
owe_obs_GetOutputStatus_outputActive{outputName="adv_stream"} 0
# TYPE owe_obs_GetOutputStatus_outputReconnecting gauge
owe_obs_GetOutputStatus_outputReconnecting{outputName="adv_stream"} 0
# TYPE owe_obs_GetOutputStatus_outputDuration gauge
owe_obs_GetOutputStatus_outputDuration{outputName="adv_stream"} 0
# TYPE owe_obs_GetOutputStatus_outputCongestion gauge
owe_obs_GetOutputStatus_outputCongestion{outputName="adv_stream"} 0
# TYPE owe_obs_GetOutputStatus_outputBytes counter
owe_obs_GetOutputStatus_outputBytes{outputName="adv_stream"} 20333009
# TYPE owe_obs_GetOutputStatus_outputSkippedFrames counter
owe_obs_GetOutputStatus_outputSkippedFrames{outputName="adv_stream"} 0
# TYPE owe_obs_GetOutputStatus_outputTotalFrames counter
owe_obs_GetOutputStatus_outputTotalFrames{outputName="adv_stream"} 1951
# TYPE owe_obs_GetOutputList_outputHeight gauge
owe_obs_GetOutputList_outputHeight{outputName="adv_file_output"} 720
# TYPE owe_obs_GetOutputList_outputWidth gauge
owe_obs_GetOutputList_outputWidth{outputName="adv_file_output"} 1280
# TYPE owe_obs_GetOutputStatus_outputActive gauge
owe_obs_GetOutputStatus_outputActive{outputName="adv_file_output"} 0
# TYPE owe_obs_GetOutputStatus_outputReconnecting gauge
owe_obs_GetOutputStatus_outputReconnecting{outputName="adv_file_output"} 0
# TYPE owe_obs_GetOutputStatus_outputDuration gauge
owe_obs_GetOutputStatus_outputDuration{outputName="adv_file_output"} 0
# TYPE owe_obs_GetOutputStatus_outputCongestion gauge
owe_obs_GetOutputStatus_outputCongestion{outputName="adv_file_output"} 0
# TYPE owe_obs_GetOutputStatus_outputBytes counter
owe_obs_GetOutputStatus_outputBytes{outputName="adv_file_output"} 61711594
# TYPE owe_obs_GetOutputStatus_outputSkippedFrames counter
owe_obs_GetOutputStatus_outputSkippedFrames{outputName="adv_file_output"} 0
# TYPE owe_obs_GetOutputStatus_outputTotalFrames counter
owe_obs_GetOutputStatus_outputTotalFrames{outputName="adv_file_output"} 5922
```

## Alternatives

* [`obs_studio_exporter`](https://github.com/lukegb/obs_studio_exporter)
