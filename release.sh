set -eua
PROJECT_NAME="obs-websocket-exporter"
REPO="Benedikt.Seidl/$PROJECT_NAME"
VERSION=$(cargo read-manifest | jq -r '.version')

LINUX_TARGET="x86_64-unknown-linux-gnu"
WINDOWS_TARGET="x86_64-pc-windows-gnu"

LINUX_RELEASE="target/$LINUX_TARGET/release/$PROJECT_NAME"
WINDOWS_RELEASE="target/$WINDOWS_TARGET/release/$PROJECT_NAME.exe"

if output=$(git status --porcelain) && [ -z "$output" ]; then
    cross build --release --target $LINUX_TARGET
    cross build --release --target $WINDOWS_TARGET
    WINDOWS_DESTINATION="target/$WINDOWS_TARGET-$PROJECT_NAME.exe"
    LINUX_DESTINATION="target/$LINUX_TARGET-$PROJECT_NAME"
    cp $LINUX_RELEASE $LINUX_DESTINATION
    cp $WINDOWS_RELEASE $WINDOWS_DESTINATION
    tea releases --repo $REPO create --tag $VERSION --target main --title $VERSION --asset $LINUX_DESTINATION --asset $WINDOWS_DESTINATION
else
  echo "git is not clean!"
  exit 1
fi
